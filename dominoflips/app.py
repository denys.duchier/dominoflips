from kivy.app import App
from .gamelayout import GameLayout

class DominoFlipsApp(App):

    def build(self):
        return GameLayout()

    def on_pause(self):
        return True
