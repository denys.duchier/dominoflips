from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ObjectProperty, BooleanProperty
from kivy.metrics    import dp

from .domino import Domino, Flipper


class Tile(object):
    
    def __init__(self, x, y):
        self._row    = y
        self._col    = x
        self._active = True
        self._domino = None

    @property
    def x(self):
        return self._col

    @property
    def y(self):
        return self._row

    @property
    def active(self):
        return self._active

    @property
    def domino(self):
        if self.active:
            return self._domino
        else:
            raise Exception("attempted access to non-active Tile")

    @domino.setter
    def domino(self, d):
        if d and self.domino:
            raise Exception("attempted to set the domino of an already occupied Tile %s" % ((self.x, self.y),))
        else:
            self._domino = d


class Col(object):
    
    def __init__(self, x, ny):
        self.elems = [Tile(x, y) for y in range(ny)]

    def __getitem__(self, y):
        return self.elems[y]


class Matrix(object):
    
    def __init__(self, nx, ny):
        self.nx = nx
        self.ny = ny
        self._matrix = [Col(x, ny) for x in range(nx)]

    def __getitem__(self, key):
        if isinstance(key, int):
            return self._matrix[key]
        return self._matrix[key[0]][key[1]]

    def __str__(self):
        return "\n".join(
            ["".join(l) for l in
             zip(*[["X" if r.active else " " for r in c] for c in self._matrix])])


class Layer(Widget):
    pass

class Board(Widget):
    hmargin = NumericProperty(dp(10))
    vmargin = NumericProperty(dp(10))
    nrows   = NumericProperty(8)
    ncols   = NumericProperty(8)
    tsize   = NumericProperty(0)
    tmargin = NumericProperty(dp(1))
    torig_x = NumericProperty(0)
    torig_y = NumericProperty(0)
    matrix  = ObjectProperty(None)
    mode    = NumericProperty(0)
    animating = BooleanProperty(False)

    MODES   = ["square", "aztec"]

    def __init__(self, **kwargs):
        super(Board, self).__init__(**kwargs)
        self.domino_layer = Layer()
        self.animation_layer = Layer()
        self.flipper_layer = Layer()
        self.add_widget(self.domino_layer)
        self.add_widget(self.flipper_layer)
        self.add_widget(self.animation_layer)
        self.dominos = []
        self.flippers = []
        self.install_mode()

    def install_mode(self):
        self.reset_matrix()
        getattr(self, self.MODES[self.mode] + "_board")()

    def reset_matrix(self):
        self.domino_layer.clear_widgets()
        self.flipper_layer.clear_widgets()
        self.animation_layer.clear_widgets()
        self.dominos = []
        self.flippers = []
        self.matrix = Matrix(self.ncols, self.nrows)

    def fill_with_dominoes(self):
        m = self.matrix
        for x in range(self.ncols):
            for y in range(self.nrows):
                t = m[x,y]
                if not t.active or t.domino:
                    continue
                d = Domino(x, y, self)
                f = Flipper(d)
                d.flipper = f
                self.dominos.append(d)
                self.flippers.append(f)
                d.register()
                self.domino_layer.add_widget(d)
                self.flipper_layer.add_widget(f)
        self.update_neighbors()

    def square_board(self):
        self.fill_with_dominoes()

    def aztec_board(self):
        nx = self.ncols/2 - 1
        y = 0
        while nx:
            for x in range(nx):
                self.matrix[x,y]._active = False
                self.matrix[-1 - x,y]._active = False
                self.matrix[x, -1 - y]._active = False
                self.matrix[-1 - x, -1 - y]._active = False
            nx -= 1
            y += 1
        self.fill_with_dominoes()

    def ok_pos(self, x, y):
        if x < 0 or x >= self.ncols:
            return None
        if y < 0 or y >= self.nrows:
            return None
        return x,y

    def ok_domino(self, x, y):
        pos = self.ok_pos(x, y)
        if pos is None:
            return None
        t = self.matrix[pos]
        if not t.active:
            return None
        return t.domino

    def update_neighbors(self):
        for d in self.dominos:
            d.update_neighbor()

    def button_larger(self):
        if self.ncols < 12:
            self.ncols += 2
            self.nrows += 2
            self.install_mode()

    def button_smaller(self):
        if self.ncols > 4:
            self.ncols -= 2
            self.nrows -= 2
            self.install_mode()

    def button_mode(self):
        self.mode = (self.mode+1) % len(self.MODES)
        self.install_mode()
