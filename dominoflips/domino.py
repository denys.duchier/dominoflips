from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty, NumericProperty, BooleanProperty, ListProperty
from kivy.metrics    import dp
from kivy.animation  import Animation
from kivy.clock      import Clock


update_must_wait = 0


class Domino(Widget):
    board    = ObjectProperty(None)
    col      = NumericProperty(0)
    row      = NumericProperty(0)
    horiz    = BooleanProperty(True)
    neighbor = ObjectProperty(None, rebind=True, allownone=True)
    flipper  = ObjectProperty(None)
    color    = ObjectProperty((0,0,0,0), rebind=True)
    rangle   = NumericProperty(0)
    rpos     = ListProperty([0,0])

    def __init__(self, col, row, board, **kwargs):
        self.board = board
        self.col = col
        self.row = row
        super(Domino, self).__init__(**kwargs)

    def deregister(self):
        self.board.matrix[self.col, self.row].domino = None
        if self.horiz:
            self.board.matrix[self.col+1, self.row].domino = None
        else:
            self.board.matrix[self.col, self.row+1].domino = None

    def register(self):
        self.board.matrix[self.col, self.row].domino = self
        if self.horiz:
            self.board.matrix[self.col+1, self.row].domino = self
        else:
            self.board.matrix[self.col, self.row+1].domino = self

    def update_neighbor(self):
        x1 = self.col
        y1 = self.row
        if self.horiz:
            y1 -= 1
        else:
            x1 += 1
        x2 = x1
        y2 = y1
        if self.horiz:
            x2 += 1
        else:
            y2 += 1
        dom1 = self.board.ok_domino(x1,y1)
        dom2 = self.board.ok_domino(x2,y2)
        self.neighbor = dom1 if dom1 and dom1 is dom2 else None

    def flip(self):
        self.deregister()
        neighbor = self.neighbor
        neighbor.deregister()
        f = self.flipper
        rpos = (f.x+f.radius, f.y+f.radius)
        global update_must_wait
        update_must_wait = 2
        def update_neighbors():
            global update_must_wait
            update_must_wait -= 1
            if not update_must_wait:
                self.board.update_neighbors()
                self.board.animating = False
        def lower(w):
            self.board.animation_layer.remove_widget(w)
            self.board.domino_layer.add_widget(w)
        if self.horiz:
            def on_complete1():
                self.rangle = 0
                self.row -= 1
                self.horiz = False
                self.register()
                update_neighbors()
                lower(self)
            def on_complete2():
                neighbor.rangle = 0
                neighbor.col += 1
                neighbor.horiz = False
                neighbor.register()
                update_neighbors()
                lower(neighbor)
        else:
            def on_complete1():
                self.rangle = 0
                self.row += 1
                self.horiz = True
                self.register()
                update_neighbors()
                lower(self)
            def on_complete2():
                neighbor.rangle = 0
                neighbor.col -= 1
                neighbor.horiz = True
                neighbor.register()
                update_neighbors()
                lower(neighbor)
        self.rpos = rpos
        neighbor.rpos = rpos
        o1 = Animation(opacity=0.6, duration=0.3)+Animation(duration=0.4)+Animation(opacity=1, duration=0.3)
        o2 = Animation(opacity=0.6, duration=0.3)+Animation(duration=0.4)+Animation(opacity=1, duration=0.3)
        a1 = FlipAnimation(rangle=90 if self.horiz else -90, duration=1, on_complete=on_complete1)&o1
        a2 = FlipAnimation(rangle=90 if self.horiz else -90, duration=1, on_complete=on_complete2)&o2
        # pop these widgets to the front
        b = self.board
        b.domino_layer.remove_widget(self)
        b.domino_layer.remove_widget(neighbor)
        b.animation_layer.add_widget(self)
        b.animation_layer.add_widget(neighbor)
        b.animating = True
        a1.start(self)
        a2.start(neighbor)


class Flipper(Widget):
    domino = ObjectProperty(None)
    radius = NumericProperty(dp(20))
    color  = ObjectProperty((0,0,0,0))

    def __init__(self, dom, **kwargs):
        self.domino = dom
        super(Flipper, self).__init__(**kwargs)

    def collide_point(self, x, y):
        if self.disabled:
            return False
        r  = self.radius
        dx = (self.x+r) - x
        dy = (self.y+r) - y
        d2 = dx*dx + dy*dy
        r2 = r*r                # maybe add some fuzz
        return d2 <= r2

    def on_touch_up(self, touch):
        if not self.domino.board.animating and \
           self.collide_point(touch.x, touch.y):
            self.domino.flip()
            return True


class FlipAnimation(Animation):

    def __init__(self, on_complete=None, **kwargs):
        self._on_complete = on_complete
        super(FlipAnimation, self).__init__(**kwargs)

    def on_complete(self, widget):
        self._on_complete()
